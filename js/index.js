//default task as blueprint
class Task {
  constructor(props) {
    this.id = this.generateId();
    this.name = this.securingName(props.name);
    this.isComplete = false;
  }

  generateId() {
    let newId = Math.floor(Math.random() * 100000000);
    return newId;
  }

  securingName(name) {
    if (name.length > 40) {
      return name.substring(0, 57) + "...";
    } else {
      return name;
    }
  }
}

class Todo {
  constructor(props) {
    let data = JSON.parse(localStorage.getItem("tasks"));
    this.tasks = data || [];
    this.amountTask = 0;
    this.amountTaskComponent = props.amountTaskComponent;
    this.selectedHtmlElement = props.selectedHtmlElement || document.body; //default content nanti diappend kemana
    this.render();
  }

  #setAmountTask(value) {
    //enkapsulasi
    if (typeof value == "number") {
      this.amountTask = value;
    }
  }

  getAmountTask() {
    return this.amountTask;
  }

  getTask() {
    return this.tasks;
  }

  #setTasks(data) {
    this.tasks = data;
    localStorage.setItem("tasks", JSON.stringify(data));
  }

  render() {
    let me = this;
    let data = this.getTask();
    this.selectedHtmlElement.innerHTML = "";
    let contentOnGoing = "";
    let contentComplete = "";
    let content = "";
    me.#setAmountTask(0);
    this.selectedHtmlElement.classList.add("pre-animation");
    if (data.length > 0) {
      data.forEach(function (item) {
        let styleNameComplete = "";
        let styleChecked = "";
        if (item.isComplete) {
          styleNameComplete = "line-through";
          styleChecked = "checked";
        } else {
          me.#setAmountTask(me.getAmountTask() + 1);
        }

        let style = {
          nameComplete: styleNameComplete,
          checked: styleChecked,
        };
        if (item.isComplete) {
          contentComplete += me.generateContentTask(item, style);
        } else {
          contentOnGoing += me.generateContentTask(item, style);
        }
      });
      content = contentOnGoing + contentComplete;
      setTimeout(function () {
        me.selectedHtmlElement.classList.remove("pre-animation");
      }, me.animationDelay);
      me.renderAmountTask();
    } else {
      content = `<div class="text-center text-white">
              <h1>No Task</h1>
              </div>`;
    }
    this.selectedHtmlElement.innerHTML = content;
  }

  generateContentTask(task, style) {
    let content = `
                <div class="group item-task text-white  flex gap-4 items-start justify-center bg-[#21212b] p-4 mb-4 rounded-3xl hover:text-black hover:bg-[#006d77]">
                    <div class="check-task">
                        <input
                        onchange="updateTask(${task.id})"
                        name="comments"
                        type="checkbox"
                        class="focus:ring-0 h-[30px] w-[30px] text-[#70c4bf] border-[#70c4bf] border-2 rounded-xl bg-[#21212b]  group-hover:bg-[#21212b]" ${style.checked}/>
                    </div>
                    <div class="description-task flex-grow ${style.nameComplete} group-hover:text-white">
                        ${task.name}
                    </div>
                    <div
                        data-id="${task.id}"
                        class="btn-delete-task bg-[#d62828] h-[30px] w-[30px] flex items-center justify-center rounded-xl"
                        onclick="deleteTask(${task.id})">
                        <i class="bx bx-trash text-white"></i>
                    </div>
                </div>`;
    return content;
  }

  renderAmountTask() {
    let amount = this.getAmountTask();
    this.amountTaskComponent.innerHTML = amount;
  }

  secureId(id) {
    if (typeof id == "number") {
      return true;
    }
  }

  addTask(params) {
    let task = new Task(params);
    this.tasks.unshift(task);
    this.#setTasks(this.getTask());
    this.render();
  }

  deleteTask(id) {
    let me = this;
    let isSecure = this.secureId(id);
    if (isSecure) {
      let newData = this.getTask().filter((task) => {
        return task.id !== id;
      });
      me.#setTasks(newData);
    }
  }

  updateTask(id) {
    let isSecure = this.secureId(id);
    let me = this;
    if (isSecure) {
      let tasks = this.tasks;
      tasks.forEach((item) => {
        if (item.id == id) {
          item.isComplete = !item.isComplete;
          me.#setTasks(this.getTask());
          return;
        }
      });
    }
  }
}

let targetApp = document.getElementById("panel-list-task");
let amountTaskComponent = document.getElementById("amount-task");
let app = new Todo({
  selectedHtmlElement: targetApp,
  amountTaskComponent: amountTaskComponent,
});

updateTask = (id) => {
  app.updateTask(id);
  app.render();
};

deleteTask = (id) => {
  app.deleteTask(id);
  app.render();
};

addTask = () => {
  let name = document.getElementById("input-new-task").value;
  if (name) {
    app.addTask({
      name: name,
    });
    document.getElementById("input-new-task").value = "";
  }
  app.render();
};

document
  .getElementById("input-new-task")
  .addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
      addTask();
    }
  });
